import { combineReducers } from 'redux';
import app from 'reducers/app';
import search from './searchReducer';

export default combineReducers({
  app,
  search,
});
