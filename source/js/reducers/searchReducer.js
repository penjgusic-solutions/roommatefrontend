/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import { Map } from 'immutable';

import { FETCH_SEARCH_APPEARANCES_START, FETCH_SEARCH_APPEARANCES_SUCCESS, FETCH_SEARCH_APPEARANCES_ERROR } from '../actions/ActionTypes';


const initialState = Map({
  asyncLoading: false,
  asyncError: null,
  asyncData: null,
});

const actionsMap = {
  // Async action
  [FETCH_SEARCH_APPEARANCES_START]: (state) => {
    return state.merge({
      asyncLoading: true,
      asyncError: null,
    });
  },
  [FETCH_SEARCH_APPEARANCES_ERROR]: (state, action) => {
    return state.merge({
      asyncLoading: false,
      asyncError: action.data,
    });
  },
  [FETCH_SEARCH_APPEARANCES_SUCCESS]: (state, action) => {
    return state.merge({
      asyncLoading: false,
      asyncData: action.data,
    });
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
