import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import MainNavigation from 'components/Global/MainNavigation';
import MainContentArea from 'components/Main/MainContentArea';
import MyProfile from '../../components/Profile/MyProfile';
import Messages from '../../components/Messages/Messages';
import MyAdds from '../../components/Adds/MyAdds';
import Modal from '../../components/Shared/Modal';
import Footer from '../../components/Global/Presentation/Footer';


export default class App extends Component {
  static propTypes = {
    children: PropTypes.object,
  };

  render() {
    return (
      <div>
        <BrowserRouter >
          <div id='page-contents'>
            <MainNavigation />
            <Switch >
              <Route path='/' exact component={ MainContentArea } />
              <Route path='/myProfile' exact component={ MyProfile } />
              <Route path='/messages' exact component={ Messages } />
              <Route path='/myAds' exact component={ MyAdds } />
            </Switch>
            <Modal />
          </div>
        </BrowserRouter>
        <Footer />
      </div>
    );
  }
}
