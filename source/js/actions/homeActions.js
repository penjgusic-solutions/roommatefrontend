/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import * as types from 'ActionTypes';
import * as api from '../api/homeApi';

function loadAddsAsyncStart() {
  return {
    type: types.FETCH_ASYNC_ADS_START,
  };
}

function loadAddsAsyncSuccess(data) {
  return {
    type: types.FETCH_ASYNC_ADS_SUCCESS,
    data,
  };
}

function loadAddsAsyncError(error) {
  return {
    type: types.FETCH_ASYNC_ADS_ERROR,
    error
  }
}


export function loadAsyncAdds(data){
  return function (dispatch) {

    dispatch(loadAddsAsyncStart());

    api.getOffersList().then(data =>{
      dispatch(loadAddsAsyncSuccess(data));

    }).catch(err =>{
      dispatch(loadAddsAsyncError(err));
    });

  }
}
