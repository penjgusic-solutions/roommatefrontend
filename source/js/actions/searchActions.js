/**
 * Created by Jurica Penjgusic on 10/11/17.
 * Email: jurica.penjgusic@outlook.com
 */


import { FETCH_SEARCH_APPEARANCES_ERROR, FETCH_SEARCH_APPEARANCES_SUCCESS, FETCH_SEARCH_APPEARANCES_START } from './ActionTypes';
import * as api from '../api/searchApi';


function searchAsyncStart() {
  return {
    type: FETCH_SEARCH_APPEARANCES_START,
  };
}

function searchAsyncSuccess(data) {
  return {
    type: FETCH_SEARCH_APPEARANCES_SUCCESS,
    data,
  };
}

function searchAsyncError(error) {
  return {
    type: FETCH_SEARCH_APPEARANCES_ERROR,
    error,
  };
}


export function searchAsync() {
  return function (dispatch) {
    dispatch(searchAsyncStart());

    api.getRecentSearches().then(data => {
      dispatch(searchAsyncSuccess(data));
    })
      .catch(err => {
        dispatch(searchAsyncError(err));
      });
  };
}
