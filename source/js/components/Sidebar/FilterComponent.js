/**
 * Created by jpe on 14.8.2017..
 */
import React from 'react';

export default class FilterComponent extends React.Component {


  render() {
    return (
      <div className='suggestions' id='sticky-sidebar'>
        <h4 className='grey'>Filtriraj</h4>

        <form name='filter' id='filter' className='form-inline'>
          <div className='follow-user edit-block'>
            <div className='form-group'>
              <label htmlFor='city' />
              <select className='form-control' id='city'>
                <option value='city'>Odaberite grad</option>
                <option>Split</option>
                <option>Rijeka</option>
                <option>Osijek</option>
                <option>Zagreb</option>
                <option>Varaždin</option>
                <option>Vukovar</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option defaultValue='Dec' value='Dec'>Dec</option>
              </select>
            </div>
          </div>
          <div className='follow-user edit-block'>
            <div className='form-group'>
              <label htmlFor='kvart' />
              <select className='form-control' id='kvart'>
                <option value='city'>Odaberite kvart</option>
                <option>Split</option>
                <option>Rijeka</option>
                <option>Osijek</option>
                <option>Zagreb</option>
                <option>Varaždin</option>
                <option>Vukovar</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option defaultValue='Dec' value='Dec'>Dec</option>
              </select>
            </div>
          </div>

          <div className='follow-user'>
            <div>
              <h4 className='grey'>Spol cimera</h4>
            </div>
            <div className='form-group'>
              <input id='male' type='checkbox' />
              <label htmlFor='male'><span style={ { fontWeight: 'normal', fontSize: '15px' } }> Muški </span></label>
            </div>
            <div className='form-group'>
              <input id='female' type='checkbox' />
              <label htmlFor='female'><span style={ { fontWeight: 'normal', fontSize: '15px' } }> Ženski </span></label>
            </div>
          </div>

        </form>
      </div>
    );
  }
}
