/**
 * Created by jpe on 31.7.2017..
 */

import React from 'react';
import { Link } from 'react-router-dom';
import StickyDiv from 'react-stickydiv';
import $ from 'jquery';

class ProfileCard extends React.Component {

  constructor() {
    super();

    this.state = {
      userName: 'Jurica Penjgusic',
      profilePhotoSrc: 'https://media-exp2.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAi8AAAAJGI4NjFkZWVmLWRmMzEtNGJiYS05YWU5LTQxMmMxMzJmNDAxYw.jpg',
    };

    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions() {
    this.setState({ width: $(window).width(), height: $(window).height() });
  }

  render() {
    if (window.innerWidth <= 992) return null;

    return (
      <StickyDiv offsetTop={ 70 } zIndex={ 2 } >
        <div id='profile-card' className='profile-card'>
          <img src={ this.state.profilePhotoSrc } alt='user' className='profile-photo' />
          <h5><Link to='/myProfile' className='text-white'>{ this.state.userName }</Link></h5>
        </div>
      </StickyDiv>
    );
  }
}

export default ProfileCard;
