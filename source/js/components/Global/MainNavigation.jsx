import React, { Component } from 'react';
import $ from 'jquery';
import logo from '../Global/../../../assets/images/logo.png';
import '../../../assets/css/style.css';
import Navbar from './Presentation/Navbar';


export default class MainNavigation extends Component {

  constructor() {
    super();

    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions() {
    this.setState({ width: $(window).width(), height: $(window).height() });
  }


  render() {
    const width = window.innerWidth;

    return (
      <Navbar width={ width } logo={ logo } searchResultList={ undefined } />
    );
  }
}

MainNavigation.propTypes = {

};

