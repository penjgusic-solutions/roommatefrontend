/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */
import React from 'react';
import { connect } from 'react-redux';
import { searchAsync } from '../../actions/searchActions';
import SearchResultList from './Presentation/SearchResultList';

class SearchForm extends React.Component {

  constructor() {
    super();

    this.state = {
      hasFocus: false,
    };

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onFocus() {
    this.setState({
      hasFocus: true,
    });
    this.props.getSearches();
  }

  onBlur() {
    this.setState({
      hasFocus: false,
    });
  }

  render() {
    return (
      <form className={ this.props.width > 768 ? 'navbar-form' : '' }>
        <div className='form-group col-lg-offset-3'>
          <i className='icon ion-android-search' />
          <input type='text' className='form-control' placeholder='Search friends, photos, videos' onFocus={ this.onFocus } onBlur={ this.onBlur } />
          <SearchResultList resultList={ this.props.items } hasFocus={ this.state.hasFocus } />
        </div>
      </form>
    );
  }
}

SearchForm.propTypes = {
  width: React.PropTypes.number.isRequired,
  items: React.PropTypes.object,
  getSearches: React.PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  items: state.search.get('asyncData'),
});

const mapDispatchToProps = dispatch => ({
  getSearches: () => dispatch(searchAsync()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
