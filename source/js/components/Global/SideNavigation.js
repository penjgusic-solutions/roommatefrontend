/**
 * Created by jpe on 18.10.2017..
 */

import React from 'react';

export default class SidebarNavigation extends React.Component {

  render() {
    return (
      <ul className='nav-news-feed hidden-xs hidden-sm'>
        <li><i className='icon ion-ios-paper' /><div><a href='newsfeed.html'>My Newsfeed</a></div></li>
        <li><i className='icon ion-ios-people' /><div><a href='newsfeed-people-nearby.html'>People Nearby</a></div></li>
        <li><i className='icon ion-ios-people-outline' /><div><a href='newsfeed-friends.html'>Friends</a></div></li>
        <li><i className='icon ion-chatboxes' /><div><a href='newsfeed-messages.html'>Messages</a></div></li>
        <li><i className='icon ion-images' /><div><a href='newsfeed-images.html'>Images</a></div></li>
        <li><i className='icon ion-ios-videocam' /><div><a href='newsfeed-videos.html'>Videos</a></div></li>
      </ul>
    );
  }
}
