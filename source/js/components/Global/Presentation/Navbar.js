/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import React from 'react';
import { Link } from 'react-router-dom';
import SearchForm from '../SearchForm';


const Navbar = ({ width, logo, searchResultList }) => {

  return (
    <header id='header'>
      <nav className='navbar navbar-default navbar-fixed-top menu'>
        <div className='container-fluid'>

          <div className='row'>

            <div className='col-sm-4 hidden-lg hidden-md hidden-xs'>
              <Link to='/' className='navbar-brand pull-right'><img src={ logo } alt='logo' /></Link>
            </div>

            <div className='col-sm-4 hidden-lg hidden-md hidden-xs'>
              <SearchForm width={ width } />
            </div>

            <div className='col-lg-4 col-sm-4 col-md-3 col-xs-2'>
              <button
                type='button' className='navbar-toggle collapsed' data-toggle='collapse'
                data-target='#bs-example-navbar-collapse-1' aria-expanded='false'
              >
                <span className='sr-only'>Toggle navigation</span>
                <span className='icon-bar' />
                <span className='icon-bar' />
                <span className='icon-bar' />
              </button>
              <Link to='/' className='navbar-brand hidden-sm hidden-xs pull-right'><img src={ logo } alt='logo' /></Link>
            </div>

            <div className='col-lg-4 col-sm-4 col-md-3 col-xs-10 hidden-sm'>
              <SearchForm width={ width } searchResultList={ searchResultList } />
            </div>

            <div className='col-lg-4 col-sm-12 col-md-6 col-xs-12'>
              <div className='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                <ul className='nav navbar-nav main-menu'>
                  <li className='dropdown'><Link to='/'>Početna</Link></li>
                  <li className='dropdown'><Link to='/myAds'>Moj oglasi</Link></li>
                  <li className='dropdown'><Link to='/messages'>Poruke</Link></li>
                  <li className='dropdown'><Link to='/myProfile'>Moj profil</Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

Navbar.propTypes = {
  width: React.PropTypes.number.isRequired,
  logo: React.PropTypes.string.isRequired,
  searchResultList: React.PropTypes.array,
};


export default Navbar;
