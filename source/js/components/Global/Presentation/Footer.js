/**
 * Created by jpe on 12.10.2017..
 */

import React from 'react';

const Footer = () => {
  return (
    <footer id='footer'>
      <div className='container'>
        <div className='row'>
          <div className='footer-wrapper'>
            <div className='col-md-3 col-sm-3'>
              <a href=''><img src='images/logo-black.png' alt='' className='footer-logo' /></a>
              <ul className='list-inline social-icons'>
                <li><a href='#'><i className='icon ion-social-facebook' /></a></li>
                <li><a href='#'><i className='icon ion-social-twitter' /></a></li>
                <li><a href='#'><i className='icon ion-social-googleplus' /></a></li>
                <li><a href='#'><i className='icon ion-social-pinterest' /></a></li>
                <li><a href='#'><i className='icon ion-social-linkedin' /></a></li>
              </ul>
            </div>
            <div className='col-md-2 col-sm-2'>
              <h5>About</h5>
              <ul className='footer-links'>
                <li><a href=''>About us</a></li>
                <li><a href=''>Contact us</a></li>
                <li><a href=''>Privacy Policy</a></li>
                <li><a href=''>Terms</a></li>
                <li><a href=''>Help</a></li>
              </ul>
            </div>
            <div className='col-md-3 col-sm-3'>
              <h5>Kontaktiraj nas</h5>
              <ul className='contact'>
                <li><i className='icon ion-ios-telephone-outline' />+385 95 546 5698</li>
                <li><i className='icon ion-ios-email-outline' />jurica.penjgusic@outlook.com</li>
                <li><i className='icon ion-ios-location-outline' />Požeška 5, Split</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className='copyright'>
        <p>Penjgušić &nbsp; Solutions &nbsp; © 2016.&nbsp; All &nbsp; rights &nbsp; reserved</p>
      </div>
    </footer>
  );
};
export default Footer;
