/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import React from 'react';


const SearchList = ({ resultList, hasFocus }) => {

  if (!hasFocus) return null;

  return (
    <ul id='results' className='results col-lg-12'>

      {
        resultList ? (
          resultList.map(item => {
            return (
              <li key={ item.get('searchId') }>
                <a className='btn text-gray' >
                  <i className='fa fa-search fa-lg' />
                  <span className='result-margin'>{ item.get('text') }</span>
                </a>
              </li>
            );
          })
        ) : null
      }

      <li><hr className='result-divider' /></li>

      <li>
        <a className='btn text-gray'>
          <i className='fa fa-users fa-lg text-blue fa-outline custom-fa' aria-hidden='true' />
          <span className='result-margin result-search-with-filters '>Search people with filters</span>
        </a>
      </li>
    </ul>
  );
};

SearchList.propTypes = {
  resultList: React.PropTypes.object,
  hasFocus: React.PropTypes.bool.isRequired,
};


export default SearchList;
