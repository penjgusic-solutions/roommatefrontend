/**
 * Created by jpe on 18.10.2017..
 */

import React from 'react';

export default class CreatePost extends React.Component {

  constructor() {
    super();

    this.publishPost = this.publishPost.bind(this);
  }

  publishPost(fileList) {

    for (let i = 0; i < fileList.length; i++) {
      console.log(fileList[i]);
    }
  }

  render() {
    return (
      <div className='create-post'>
        <div className='row'>
          <div className='col-md-7 col-sm-7'>
            <div className='form-group'>
              <img src='http://placehold.it/300x300' alt='' className='profile-photo-md' />
              <textarea
                name='texts' id='exampleTextarea' cols='30' rows='1' className='form-control'
                placeholder='Write what you wish'
              />
            </div>
          </div>
          <div className='col-md-5 col-sm-5'>
            <div className='tools'>
              <ul className='publishing-tools list-inline'>
                <li><a data-toggle='modal' data-target='#myModal'><i className='ion-compose' /></a></li>
                <li className='image-upload'>
                  <label htmlFor='image-input'>
                    <a>
                      <i className='ion-images' />
                    </a>
                  </label>
                  <input id='image-input' type='file' multiple onChange={ (e) => this.publishPost(e.target.files) } />
                </li>
                <li><a href=''><i className='icon ion-map' /></a></li>
              </ul>
              <button className='btn btn-primary pull-right'>Publish</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
