/**
 * Created by jpe on 18.10.2017..
 */

import React from 'react';
import ProfileCard from '../Global/ProfileCard';
import ChatOnline from '../Chat/ChatOnlineComponent';
import CreatePost from '../Posts/CreatePost';
import ChatRoom from '../Chat/ChatRoom';
import SidebarNavigation from '../Global/SideNavigation';

export default class MessagesComponent extends React.Component {

  render() {
    return (
      <div className='container'>
        <div className='row'>

          <div className='col-md-3 static'>
            <ProfileCard />
            <SidebarNavigation />
            <ChatOnline />
          </div>

          <div className='col-md-7'>
            <CreatePost />
            <ChatRoom />
          </div>

        </div>
      </div>
    );
  }
}
