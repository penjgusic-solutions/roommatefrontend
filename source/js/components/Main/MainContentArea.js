/**
 * Created by jpe on 31.7.2017..
 */

import React from 'react';
import ProfileCard from '../Global/ProfileCard';
import Posts from '../Posts/Posts';
import FilterComponent from '../Sidebar/FilterComponent';
import CreatePost from '../Posts/CreatePost';
import Modal from '../Shared/Modal';

export default class MainContentArea extends React.Component {

  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-3 static'>
            <ProfileCard />
          </div>
          <div className='col-md-7'>
            <CreatePost />
            <Posts />
          </div>
          <div className='col-md-2 static'>
            <FilterComponent />
          </div>
        </div>
      </div>
    );
  }
}
