/**
 * Created by jpe on 18.10.2017..
 */

import React from 'react';


export default class ChatOnlineComponent extends React.Component {

  render() {
    return (
      <div id='chat-block'>
        <div className='title'>Chat online</div>
        <ul className='online-users list-inline'>
          <li><a href='newsfeed-messages.html' title='Linda Lohan'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Sophia Lee'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='John Doe'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Alexis Clark'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='James Carter'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Robert Cook'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Richard Bell'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Anna Young'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
          <li><a href='newsfeed-messages.html' title='Julia Cox'><img src='http://placehold.it/300x300' alt='user' className='img-responsive profile-photo' /><span className='online-dot' /></a></li>
        </ul>
      </div>
    );
  }
}
