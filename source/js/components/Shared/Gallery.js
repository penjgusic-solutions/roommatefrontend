/**
 * Created by jpe on 20.10.2017..
 */

import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import Lightbox from 'react-images';
import PropTypes from 'prop-types';

const gutter = {
  small: 2,
  large: 4,
};


const classes = StyleSheet.create({
  gallery: {
    marginRight: -gutter.small,
    overflow: 'hidden',

    '@media (min-width: 500px)': {
      marginRight: -gutter.large,
    },
  },

  // anchor
  thumbnail: {
    boxSizing: 'border-box',
    display: 'block',
    float: 'left',
    lineHeight: 0,
    paddingRight: gutter.small,
    paddingBottom: gutter.small,
    overflow: 'hidden',

    '@media (min-width: 500px)': {
      paddingRight: gutter.large,
      paddingBottom: gutter.large,
    },
  },

  // orientation
  landscape: {
    width: '30%',
  },
  square: {
    paddingBottom: 0,
    width: '40%',

    '@media (min-width: 500px)': {
      paddingBottom: 0,
    },
  },

  // actual <img />
  source: {
    border: 0,
    display: 'block',
    height: 'auto',
    maxWidth: '100%',
    width: 'auto',
  },
});


class Gallery extends React.Component {

  constructor() {
    super();

    this.state = {
      lightBoxIsOpen: false,
      currentImage: 0,
    };

    this.closeLightBox = this.closeLightBox.bind(this);
    this.goToNext = this.goToNext.bind(this);
    this.goToPrevious = this.goToPrevious.bind(this);
    this.goToImage = this.goToImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightBox = this.openLightBox.bind(this);
  }

  openLightBox(index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightBoxIsOpen: true,
    });
  }
  closeLightBox() {
    this.setState({
      currentImage: 0,
      lightBoxIsOpen: false,
    });
  }
  goToPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  goToNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  goToImage(index) {
    this.setState({
      currentImage: index,
    });
  }
  handleClickImage() {
    if (this.state.currentImage === this.props.images.length - 1) return;

    this.goToNext();
  }

  renderGallery() {
    const { images } = this.props;

    if (!images) return;

    const gallery = images.filter(i => i.useForDemo).map((obj, i) => {
      return (
        <a
          href={ obj.src }
          className={ css(classes.thumbnail, classes[obj.orientation]) }
          key={ i }
          onClick={ (e) => this.openLightBox(i, e) }
        >
          <img src={ obj.thumbnail } className={ css(classes.source) } role='presentation' />
        </a>
      );
    });

    return (
      <div className={ css(classes.gallery) }>
        {gallery}
      </div>
    );
  }
  render() {
    return (
      <div className='section'>
        {this.props.heading && <h2>{this.props.heading}</h2>}
        {this.props.subheading && <p>{this.props.subheading}</p>}
        {this.renderGallery()}
        <Lightbox
          currentImage={ this.state.currentImage }
          images={ this.props.images }
          isOpen={ this.state.lightBoxIsOpen }
          onClickImage={ this.handleClickImage }
          onClickNext={ this.goToNext }
          onClickPrev={ this.goToPrevious }
          onClickThumbnail={ this.goToImage }
          onClose={ this.closeLightBox }
          showThumbnails={ this.props.showThumbnails }
          theme={ this.props.theme }
        />
      </div>
    );
  }
}

Gallery.displayName = 'Gallery';
Gallery.propTypes = {
  heading: PropTypes.string,
  images: PropTypes.array,
  showThumbnails: PropTypes.bool,
  subheading: PropTypes.string,
  theme: PropTypes.object,
};

export default Gallery;
