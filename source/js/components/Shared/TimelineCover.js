/**
 * Created by jpe on 20.10.2017..
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Lightbox from 'react-images';

export default class TimeLineCover extends React.Component {

  constructor(props) {
    super(props);

    this.getInitialState();

    this.coverShow = this.coverShow.bind(this);
    this.closeLightBox = this.closeLightBox.bind(this);
    this.goToPrevious = this.goToPrevious.bind(this);
    this.goToNext = this.goToNext.bind(this);
    this.profilePictureShow = this.profilePictureShow.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
  }

  getInitialState() {
    this.state = {
      showThumbnails: false,
      images: [{
        src: this.props.timeLineCover,
        thumbnail: this.props.timeLineCover,
        srcset: [
          this.props.timeLineCover,
        ],
      }, {
        src: this.props.profilePhotoSrc,
        thumbnail: this.props.profilePhotoSrc,
        srcset: [
          this.props.profilePhotoSrc,
        ],
      }],
      photoIndex: 0,
      isOpen: false,
    };
  }

  goToPrevious() {
    this.setState({
      photoIndex: this.state.photoIndex - 1,
    });
  }
  goToNext() {
    this.setState({
      photoIndex: this.state.photoIndex + 1,
    });
  }

  handleClickImage() {
    if (this.state.photoIndex === this.state.images.length - 2) return;

    this.goToPrevious();
  }

  closeLightBox() {
    this.setState({
      isOpen: false,
      photoIndex: 0,
    });
    document.body.style.paddingRight = null;
    document.body.style.overflowY = null;
  }

  coverShow() {
    this.setState({
      isOpen: true,
      photoIndex: 0,
    });
  }

  profilePictureShow() {
    this.setState({
      photoIndex: 1,
      isOpen: true,
    });
  }

  render() {
    const { profilePhotoSrc, userName, timeLineCover, title } = this.props;

    const { isOpen, images, photoIndex, showThumbnails } = this.state;

    return (
      <div className='timeline-cover' style={ { backgroundImage: `url('${ timeLineCover }')` } } >
        {
          isOpen &&
            <Lightbox
              currentImage={ photoIndex }
              images={ images }
              isOpen={ isOpen }
              onClickImage={ this.handleClickImage }
              onClickNext={ this.goToNext }
              onClickPrev={ this.goToPrevious }
              onClickThumbnail={ this.goToImage }
              onClose={ this.closeLightBox }
              showThumbnails={ showThumbnails }
            />
        }

        <div className='timeline-nav-bar hidden-sm hidden-xs'>
          <div className='row'>
            <div className='col-md-3'>
              <div className='profile-info'>
                <img src={ profilePhotoSrc } alt='' className='img-responsive profile-photo' onClick={ this.profilePictureShow } />
                <h3>{ userName }</h3>
                <p className='text-muted'>{ title }</p>
              </div>
            </div>
            <div className='col-md-9'>
              <ul className='list-inline profile-menu'>
                <li><Link to='/myProfile' > Timeline </Link></li>
                <li><Link to={ `/profile/${ 1 }` }>About</Link></li>
              </ul>
            </div>
          </div>
        </div>

        <div className='navbar-mobile hidden-lg hidden-md'>
          <div className='profile-info'>
            <img src={ profilePhotoSrc } alt='' className='img-responsive profile-photo' onClick={ this.profilePictureShow } />
            <h4>{ userName }</h4>
            <p className='text-muted'>{ title }</p>
          </div>
          <div className='mobile-menu'>
            <ul className='list-inline'>
              <li><Link to='/myProfile' > Timeline </Link></li>
              <li><Link to={ `/profile/${ 1 }` }>About</Link></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

TimeLineCover.displayName = 'TimeLineCover';
TimeLineCover.propTypes = {
  userName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  profilePhotoSrc: PropTypes.string,
  timeLineCover: PropTypes.string,
};

