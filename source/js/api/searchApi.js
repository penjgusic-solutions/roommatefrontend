/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import 'es6-promise';

export function getRecentSearches() {
  return new Promise(resolve => {
    setTimeout(() => {
      const data = [
        {
          searchId: 100,
          text: 'Ivan Ivic',
        },
        {
          searchId: 101,
          text: 'Ivan Ivic',
        },
        {
          searchId: 102,
          text: 'Ivan Ivic',
        },
        {
          searchId: 103,
          text: 'Ivan Ivic',
        },
        {
          searchId: 104,
          text: 'Ivan Ivic',
        },
        {
          searchId: 105,
          text: 'Ivan Ivic',
        },
      ];
      resolve(data);
    }, (Math.random() * 1000) + 1000); // 1-2 seconds delay
  });
}

