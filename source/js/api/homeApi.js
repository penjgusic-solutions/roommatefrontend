/**
 * Created by Jurica Penjgusic on 10/10/17.
 * Email: jurica.penjgusic@outlook.com
 */

import 'es6-promise';

export function getOffersList() {
  return new Promise(resolve => {
    setTimeout(() => {
      const data = {
        userId: 190,
        userFirstName: 'Ivan',
        lastName: 'Ivic',
        shortDescription: 'Trazim cimera u stanu',
        images: [
          'http:someImagesurl0',
          'http:someImagesurl1',
          'http:someImagesurl2',
        ],
      };
      resolve(data);
    }, (Math.random() * 1000) + 1000); // 1-2 seconds delay
  });
}

